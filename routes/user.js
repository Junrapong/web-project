const express = require("express");
const router = express.Router();
const path = require("path");
const session = require("express-session");


router.get("/user", function (req, res) {
  res.json({
    userID: req.session.userID,
    fname_lname: req.session.Fname_Lname,
    username: req.session.Username,
    studentID: req.session.StudentID,
    email: req.session.Email,
    role: req.session.Role,
  });
});

// ------------- Update a user --------------
router.put("/user/:id", function (req, res) {
  const id = req.params.id;
  // const {name, price, amount} = req.body;
  // const updateProduct = {name: name, price: price, amount: amount};
  const updateUser = req.body;

  const sql = "UPDATE user SET ? WHERE UserID = ?";
  con.query(sql, [updateUser, id], function (err, results) {
    if (err) {
      console.error(err);
      return res.status(500).send("Database server error");
    }
    if (results.affectedRows != 1) {
      console.error("Row updated is not 1");
      return res.status(500).send("Update failed");
    }
    res.send("Update succesfully");
  });
});

router.get("/user/home", function (req, res) {
  if (req.session.Role !== "user") {
    res.redirect("/");
  } else {
    res.sendFile(path.join(__dirname, "../views/User/items.html"));
  }
});

router.get("/user/items", function (req, res) {
  if (req.session.Role !== "user") {
    res.redirect("/");
  } else {
    res.sendFile(path.join(__dirname, "../views/User/items.html"));
  }
});

router.get("/user/detail", function (req, res) {
  if (req.session.Role !== "user") {
    res.redirect("/");
  } else {
    res.sendFile(path.join(__dirname, "../views/User/detail.html"));
  }
});

router.get("/user/reserve", function (req, res) {
  if (req.session.Role !== "user") {
    res.redirect("/");
  } else {
    res.sendFile(path.join(__dirname, "../views/User/reserve.html"));
  }
});

router.get("/user/status", function (req, res) {
  if (req.session.Role !== "user") {
    res.redirect("/");
  } else {
    res.sendFile(path.join(__dirname, "../views/User/status.html"));
  }
});

router.get("/user/history", function (req, res) {
  if (req.session.Role !== "user") {
    res.redirect("/");
  } else {
    res.sendFile(path.join(__dirname, "../views/User/history.html"));
  }
});

router.get("/user/profile", function (req, res) {
  if (req.session.Role !== "user") {
    res.redirect("/");
  } else {
    res.sendFile(path.join(__dirname, "../views/User/edit_profile.html"));
  }
});

router.get("/user/security", function (req, res) {
  if (req.session.Role !== "user") {
    res.redirect("/");
  } else {
    res.sendFile(path.join(__dirname, "../views/User/security.html"));
  }
});

router.get("/register", function (req, res) {
  if (req.session.Role) {
    return res.redirect('/');
  }
    res.sendFile(path.join(__dirname, "../views/Main/register.html"));
});

router.get("/forgotpassword", function (req, res) {
    res.sendFile(path.join(__dirname, "../views/Main/forgot_password.html"));
});

module.exports = router;
