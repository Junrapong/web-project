const path = require("path");
const express = require("express");
const router = express.Router();
const bcrypt = require("bcrypt");
const con = require("../config/db");

// ------------- Register --------------
router.post("/register", function (req, res) {
  const {
    Email,
    Username,
    Password,
    Fname_Lname,
    Phone_Number,
    Student_ID,
    Role,
  } = req.body;
  bcrypt.hash(Password, 10, function (err, hash) {
    if (err) {
      return res.status(500).json({ error: "Password hashing error" });
    }
    const checkEmailQuery = "SELECT * FROM user WHERE Email = ?";
    con.query(checkEmailQuery, [Email], function (err, result) {
      if (err) {
        res.status(500).json({ error: "Database error" });
      } else if (result.length > 0) {
        res.status(400).json({ error: "Email already exists" });
      } else {
        const inserUserQuery =
          "INSERT INTO user (Email, Username, Password, Fname_Lname, Phone_Number, Student_ID, Role) VALUES(?, ?, ?, ?, ?, ?, 'user')";
        con.query(
          inserUserQuery,
          [Email, Username, hash, Fname_Lname, Phone_Number, Student_ID],
          function (err) {
            if (err) {
              res.status(500).json({ error: "Database error" });
            } else {
              res.send("/");
            }
          }
        );
      }
    });
  });
});

module.exports = router;
