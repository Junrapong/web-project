const express = require("express");
const router = express.Router();
const path = require("path");
const multer = require("multer");
const con = require("../config/db");

const storage = multer.memoryStorage();
const upload = multer({ storage: storage });

//------------- GET all products --------------
router.get("/items", function (req, res) {
  const sql = "SELECT * FROM items";
  con.query(sql, function (err, results) {
    if (err) {
      console.error(err);
      return res.status(500).send("Database server error");
    }
    res.json(results);
  });
});

// ------------- GET id products --------------
// router.get("/items/:id", function (req, res) {
//   const Id = req.params.id;
//   const sql = "SELECT * FROM items WHERE Status = ?";
//   con.query(sql, [id], function (err, results) {
//     if (err) {
//       console.error(err);
//       return res.status(500).send("Database server error");
//     }
//     res.json(results);
//   });
// });

// ------------- Delete a product --------------
router.delete("/items/:id", function (req, res) {
  const id = req.params.id;
  const sql = "DELETE FROM items WHERE itemID = ?";
  con.query(sql, [id], function (err, results) {
    if (err) {
      console.error(err);
      return res.status(500).send("Database server error");
    }
    if (results.affectedRows != 1) {
      console.error("Row deleted is not 1");
      return res.status(500).send("Delete failed");
    }
    res.send("Delete succesfully");
  });
});

// ------------- Add a new product --------------
router.post("/items", function (req, res) {
  // const {name, price, amount} = req.body;
  // const newProduct = {name: name, price: price, amount: amount};
  const newProduct = req.body;

  const sql = "INSERT INTO items  SET ?";
  con.query(sql, newProduct, function (err, results) {
    if (err) {
      console.error(err);
      return res.status(500).send("Database server error");
    }
    if (results.affectedRows != 1) {
      console.error("Row added is not 1");
      return res.status(500).send("Add failed");
    }
    res.send("Add succesfully");
  });
});

// ------------- Open And Close Item --------------
router.post("/OpenAndCloseItem", function (req, res) {
  if (req.session.role != undefined && req.session.role == "staff") {
    let sql = "";
    let checkstatus = req.body.status;
    if (checkstatus == "available") {
      sql = "UPDATE items SET Status = unavailable WHERE ItemID = ?";
    } else {
      sql = "UPDATE items SET Status = available WHERE ItemID = ?";
    }
    let params = [req.body.ItemID];
    con.query(sql, params, (err, result) => {
      if (err) {
        res.status(500).send("DB Error");
        throw err;
      }
      console.log(req.session.email + 'open and close room')
      res.send('/staff/edit_item')
    });
  }
});

// ------------- Update a product --------------
router.put("/items/:id", function (req, res) {
  const id = req.params.id;
  // const {name, price, amount} = req.body;
  // const updateProduct = {name: name, price: price, amount: amount};
  const updateProduct = req.body;

  const sql = "UPDATE items SET ? WHERE itemID = ?";
  con.query(sql, [updateProduct, id], function (err, results) {
    if (err) {
      console.error(err);
      return res.status(500).send("Database server error");
    }
    if (results.affectedRows != 1) {
      console.error("Row updated is not 1");
      return res.status(500).send("Update failed");
    }
    res.send("Update succesfully");
  });
});





// ปอนทำ------------------------------


// Add-item from Staff----------------------------
router.post("/Staff/add_item/Send_Data", function (req, res) {
  if (req.session.Role != undefined && req.session.Role == "staff"){
  const {name,detail,status,imageURL} = req.body;

  const sql = "INSERT INTO items (Name,Detail,Status,ImageURL) VALUES(?, ?, ?, ?)";
  con.query(sql, [name,detail,status,imageURL], function (err, results) {
    if (err) {
      console.error(err);
      return res.status(500).send("Database server error");
    }
    if (results.affectedRows != 1) {
      console.error("Row added is not 1");
      return res.status(500).send("Add failed");
    }
    res.send("Add succesfully");
  });
}
});
// Add-item----------------------------


// get-All-item from user----------------------------
router.get("/user/items/GetAllItems", function (_req, res) {
  const sql = "SELECT * FROM items";
  con.query(sql, function (err, results) {
      if (err) {
          console.error(err);
          return res.status(500).send("Database server error");
      }
      res.json(results);
  });
});
// get-All-item from user----------------------------

// get- by id from user----------------------------
router.get("/user/items/:id", function (req, res) {
  const id = req.params.id;
  const sql = "SELECT * FROM items WHERE ItemID = ?";
  con.query(sql, [id], function (err, results) {
    if (err) {
      console.error(err);
      return res.status(500).send("Database server error");
    }
    res.json(results);
  });
});




router.get("/items/:Status", function (req, res) {
  const status = req.params.Status;
  const sql = "SELECT * FROM items WHERE Status = ?";
  con.query(sql, [status], function (err, results) {
    if (err) {
      console.error(err);
      return res.status(500).send("Database server error");
    }
    let count = results.length;
    res.json(count);
  });
});


router.get("/lender/virws_request/items/:id", function (req, res) {
  const Id = req.params.id;
  const sql = "SELECT * FROM items WHERE ItemID = ?";
  con.query(sql, [Id], function (err, results) {
    if (err) {
      console.error(err);
      return res.status(500).send("Database server error");
    }
    res.json(results);
  });
});



router.put("/Lender/virws_request/ItemID/:Itemid", function (req, res) {
  const id = req.params.Itemid;
  const sql = "UPDATE items SET Status = ? WHERE itemID = ?";
  con.query(sql, ['Borrowed', id], function (err, results) {
    if (err) {
      console.error(err);
      return res.status(500).send("Database server error");
    }
    if (results.affectedRows != 1) {
      console.error("Row updated is not 1");
      return res.status(500).send("Update failed");
    }
    res.send("Update succesfully");
  });
});


router.put("/staff/item/:id", function (req, res) {
  const id = req.params.id;
  // const {name, price, amount} = req.body;
  // const updateProduct = {name: name, price: price, amount: amount};
  const sql = "UPDATE items SET Status = ? WHERE  ItemID= ?";
  con.query(sql, ['Available', id], function (err, results) {
    if (err) {
      console.error(err);
      return res.status(500).send("Database server error");
    }
    if (results.affectedRows != 1) {
      console.error("Row updated is not 1");
      return res.status(500).send("Update failed");
    }
    res.send("Update succesfully");
  });
});

// ปอนทำ------------------------------





module.exports = router;
