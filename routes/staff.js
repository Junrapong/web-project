const express = require("express");
const router = express.Router();
const path = require("path");

router.get("/staff/home", function (req, res) {
  if (req.session.Role !== "staff") {
    res.redirect("/");
  } else {
    res.sendFile(path.join(__dirname, "../views/Staff/Return.html"));
  }
});

router.get("/staff/items", function (req, res) {
  if (req.session.Role !== "staff") {
    res.redirect("/");
  } else {
    res.sendFile(path.join(__dirname, "../views/Staff/items_staff.html"));
  }
});

router.get("/staff/dashboards", function (req, res) {
  if (req.session.Role !== "staff") {
    res.redirect("/");
  } else {
    res.sendFile(path.join(__dirname, "../views/Staff/dashboard_staff.html"));
  }
});

router.get("/staff/Return", function (req, res) {
  if (req.session.Role !== "staff") {
    res.redirect("/");
  } else {
    res.sendFile(path.join(__dirname, "../views/Staff/Return.html"));
  }
});

router.get("/staff/history", function (req, res) {
  if (req.session.Role !== "staff") {
    res.redirect("/");
  } else {
    res.sendFile(path.join(__dirname, "../views/Staff/history_staff.html"));
  }
});

router.get("/staff/detail", function (req, res) {
  if (req.session.Role !== "staff") {
    res.redirect("/");
  } else {
    res.sendFile(path.join(__dirname, "../views/Staff/detail_staff.html"));
  }
});

router.get("/staff/add_item", function (req, res) {
  if (req.session.Role !== "staff") {
    res.redirect("/");
  } else {
    res.sendFile(path.join(__dirname, "../views/Staff/add_staff.html"));
  }
});

router.get("/staff/edit_item", function (req, res) {
  if (req.session.Role !== "staff") {
    res.redirect("/");
  } else {
    res.sendFile(path.join(__dirname, "../views/Staff/edit_staff.html"));
  }
});

module.exports = router;
