const express = require("express");
const router = express.Router();
const path = require("path");

router.get("/lender/home", function (req, res) {
  if (req.session.Role !== "lender") {
    res.redirect("/");
  } else {
    res.sendFile(path.join(__dirname, "../views/Lender/request_lender.html"));
  }
});

router.get("/Lender/assetsList", function (req, res) {
  if (req.session.Role !== "lender") {
    res.redirect("/");
  } else {
    res.sendFile(path.join(__dirname, "../views/Lender/asset_list_lender.html"));
  }
});

router.get("/Lender/request", function (req, res) {
  if (req.session.Role !== "lender") {
    res.redirect("/");
  } else {
    res.sendFile(
      path.join(__dirname, "../views/Lender/request_lender.html")
    );
  }
});

router.get("/Lender/dashboard", function (req, res) {
  if (req.session.Role !== "lender") {
    res.redirect("/");
  } else {
    res.sendFile(path.join(__dirname, "../views/Lender/dashboard_lender.html"));
  }
});

router.get("/Lender/history", function (req, res) {
  if (req.session.Role !== "lender") {
    res.redirect("/");
  } else {
    res.sendFile(path.join(__dirname, "../views/Lender/history_lender.html"));
  }
});

router.get("/Lender/detail", function (req, res) {
  if (req.session.Role !== "lender") {
    res.redirect("/");
  } else {
    res.sendFile(path.join(__dirname, "../views/Lender/detail_lender.html"));
  }
});



module.exports = router;
