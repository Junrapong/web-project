const express = require("express");
const router = express.Router();
const path = require("path");
const bcrypt = require("bcrypt");
const session = require("express-session");
const MemoryStore = require("memorystore")(session);
const con = require("../config/db");

// router.use(
//   session({
//     cookie: { maxAge: 24 * 60 * 60 * 1000 }, //1 day in millisec
//     secret: "keyboardcat",
//     resave: false,
//     saveUninitialized: true,
//     store: new MemoryStore({
//       checkPeriod: 24 * 60 * 60 * 1000, // prune expired entries every 24h
//     }),
//   })
// );

// ------------- Login --------------
router.post("/login", function (req, res) {
  const Username = req.body.Username;
  const Password = req.body.Password;
  // console.log(username, password);

  const sql = "SELECT * FROM user WHERE Username = ? OR Email = ?";
  con.query(sql, [Username, Username], function (err, results) {
    if (err) {
      console.error(err);
      return res.status(500).send("Database server error");
    }
    if (results.length != 1) {
      return res.status(400).send("Wrong username");
    }
    bcrypt.compare(Password, results[0].Password, function (err, result) {
      if (err) {
        res.status(503).send("Authentication server error");
      } else if (result) {
        req.session.UserID = results[0].UserID;
        req.session.Username = Username;
        req.session.Role = results[0].Role;
        req.session.StudentID = results[0].Student_ID;
        req.session.Fname_Lname = results[0].Fname_Lname;


        if (results[0].Role == "user") {
          //correct login send destination URL to client
          // console.log(Username);
          // console.log(results[0].Role);
          // console.log(results[0].Student_ID);
          res.send("/user/home");
        } else if (results[0].Role == "staff") {
          //correct login send destination URL to client
          res.send("/staff/home");
        } else if (results[0].Role == "lender") {
          //correct login send destination URL to client
          res.send("/lender/home");
        }
      } else {
        //wrong password
        res.status(400).send("Wrong password");
      }
    });
  });
});

// ------------- login --------------
router.get("/login", function (req, res) {
  if (req.session.Role) {
    return res.redirect('/');
  }
  res.sendFile(path.join(__dirname, "../views/Main/login.html"));
});

// ------------- Logout --------------
router.get("/logout", function (req, res) {
  //clear session variable
  req.session.destroy(function (err) {
    if (err) {
      console.error(err.message);
      res.status(500).send("Cannot clear session");
    }
    // redirect to homepage
    res.redirect("/");
  });
});

module.exports = router;
