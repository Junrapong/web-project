const express = require("express");
const router = express.Router();
const path = require("path");
const con = require("../config/db");
const session = require("express-session");


// ------------- GET all borrow --------------
router.get("/borrow", function (req, res) {
  const sql = "SELECT * FROM borrow";
  con.query(sql, function (err, results) {
    if (err) {
      console.error(err);
      return res.status(500).send("Database server error");
    }
    res.json(results);
  });
});

// ------------- GET id borrow --------------
router.get("/borrow/userid", function (req, res) {
  const id = req.session.UserID;
  const sql = "SELECT * FROM borrow WHERE UserID = ?";
  con.query(sql, [id], function (err, results) {
    if (err) {
      console.error(err);
      return res.status(500).send("Database server error");
    }
    res.json(results);
  });
});

// ------------- GET show status in table borrow --------------
router.get("/borrow/:Status", function (req, res) {
  const status = req.params.Status;
  const sql = "SELECT * FROM borrow WHERE Status = ?";
  con.query(sql, [status], function (err, results) {
    if (err) {
      console.error(err);
      return res.status(500).send("Database server error");
    }
    let count = results.length;
    res.json(count);
  });
});





// ------------- Add a new borrow --------------
// router.post("/borrow", function (req, res) {
//   // const {name, price, amount} = req.body;
//   const newProduct = { name: name, price: price, amount: amount };
//   const newBorrow = req.body;

//   const sql = "INSERT INTO borrow SET ? ";
//   // let params = [
//   //   [
//   //       req.session.UserID,
//   //       req.params.id,
//   //       req.body.BorrowDate,
//   //       req.body.ReturnDate
//   //       // req.body.Reason
//   //   ]]
//   con.query(sql, newBorrow, function (err, results) {
//     if (err) {
//       console.error(err);
//       return res.status(500).send("Database server error");
//     }
//     if (results.affectedRows != 1) {
//       console.error("Row added is not 1");
//       return res.status(500).send("Add failed");
//     }
//     res.send("Add succesfully");
//   });
// });



router.post("/ApproveRequest", function (req, res) {
  if (req.session.role != undefined && req.session.role == "lender") {
    let sql = 'UPDATE borrow SET Status = ?, UserID = ? WHERE BorrowID = ?';
    let params = [
      "Approve",
      req.session.UserID,
      req.body.id
    ]
    con.query(sql, params, (err, result) => {
      if (err) {
        res.status(500).send('DB Error');
        throw err;
      }
      console.log(req.session.email + 'approve request')
      res.send('/Lender/request');
    })
  }
});

router.post("/RejectRequest", function (req, res) {
  if (req.session.role != undefined && req.session.role == "lender") {
    let sql = 'UPDATE borrow SET Status = ?, UserID = ? WHERE BorrowID = ?';
    let params = [
      "Reject",
      req.session.UserID,
      req.body.id
    ]
    con.query(sql, params, (err, result) => {
      if (err) {
        res.status(500).send('DB Error');
        throw err;
      }
      console.log(req.session.email + 'reject request')
      res.send('/Lender/request');
    })
  }
});

// ------------- Update a product --------------
router.put("/borrow/:id", function (req, res) {
  const id = req.params.id;
  // const {name, price, amount} = req.body;
  // const updateProduct = {name: name, price: price, amount: amount};
  const newBorrow = req.body;

  const sql = "UPDATE borrow SET ? WHERE BorrowID = ?";
  con.query(sql, [newBorrow, id], function (err, results) {
    if (err) {
      console.error(err);
      return res.status(500).send("Database server error");
    }
    if (results.affectedRows != 1) {
      console.error("Row updated is not 1");
      return res.status(500).send("Update failed");
    }
    res.send("Update succesfully");
  });
});


// ปอนทำ------------------------------


router.post("/borrow/addformuser", function (req, res) {
  let UserID = req.session.UserID;
  let fullname = req.session.Fname_Lname;
  let studentID = req.session.StudentID;
  const { ItemID, NameItem, BorrowDate, ReturnDate} = req.body;
  const sql = "INSERT INTO borrow (Student_ID , UserID, ItemID, NameItem, Booker ,BorrowDate, ReturnDate, Status, Reason) VALUES(?, ?, ?, ? ,?, ? ,?,'Pending','Null')";
  con.query(sql, [studentID,UserID, ItemID, NameItem, fullname, BorrowDate ,ReturnDate], function (err, results) {
    if (err) {
      console.error(err);
      return res.status(500).send("Database server error");
    }
    if (results.affectedRows != 1) {
      console.error("Row added is not 1");
      return res.status(500).send("Add failed");
    }
    res.send("Add succesfully");
  });
});



// ------------- GET borrow by Pending ststus lender--------------
router.get("/lender/virws_request/:Status", function (req, res) {
  const status = req.params.Status;
  const sql = "SELECT * FROM borrow WHERE Status = ?";
  con.query(sql, [status], function (err, results) {
    if (err) {
      console.error(err);
      return res.status(500).send("Database server error");
    }
    res.json(results);
  });
});



router.put("/Lender/virws_request/borrowApprove/:borrowid", function (req, res) {
  const lenderName = req.session.Fullname;
  const Borrowid = req.params.borrowid;
  //const sql = "UPDATE borrow (Status , ApproveName) VALUES(? ,?) WHERE BorrowID = ?";
  let sql = "UPDATE borrow SET Status = ?, ApproveName = ? WHERE BorrowID = ?";
  con.query(sql, ['Approved', lenderName, Borrowid], function (err, results) {
    if (err) {
      console.error(err);
      return res.status(500).send("Database server error");
    }
    if (results.affectedRows != 1) {
      console.error("Row updated is not 1");
      return res.status(500).send("Update failed");
    }
    res.send("Update succesfully");
  });
});


router.put("/Lender/virws_request/Reject", function (req, res) {
  const { Reason, Borrowid }=req.body;
  const lenderName = req.session.Fullname;
  //const sql = "UPDATE borrow (Status , ApproveName) VALUES(? ,?) WHERE BorrowID = ?";
  let sql = "UPDATE borrow SET Reason = ?,ApproveName = ?,Status = ? WHERE BorrowID = ?";
  con.query(sql, [Reason,lenderName ,'Rejected',Borrowid], function (err, results) {
    if (err) {
      console.error(err);
      return res.status(500).send("Database server error");
    }
    if (results.affectedRows != 1) {
      console.error("Row updated is not 1");
      return res.status(500).send("Update failed");
    }
    res.send("Update succesfully");
  });
});


router.put("/staff/borrow/:id", function (req, res) {
  const id = req.params.id;
  // const {name, price, amount} = req.body;
  // const updateProduct = {name: name, price: price, amount: amount};
  const sql = "UPDATE borrow SET Status = ? WHERE BorrowID = ?";
  con.query(sql, ['Returned', id], function (err, results) {
    if (err) {
      console.error(err);
      return res.status(500).send("Database server error");
    }
    if (results.affectedRows != 1) {
      console.error("Row updated is not 1");
      return res.status(500).send("Update failed");
    }
    res.send("Update succesfully");
  });
});

router.get("/user/status/:id", function (req, res) {
  const id = req.params.id;
  const sql = "SELECT * FROM borrow WHERE BorrowID = ?";
  con.query(sql, [id], function (err, results) {
    if (err) {
      console.error(err);
      return res.status(500).send("Database server error");
    }
    res.json(results);
  });
});

// ปอนทำ------------------------------

module.exports = router;
