const path = require("path");
const express = require("express");
const router = express.Router();
const bcrypt = require("bcrypt");
const userRoute = require("./routes/user");
const staffRoute = require("./routes/staff");
const lenderRoute = require("./routes/lender");
const loginRoute = require("./routes/login");
const registerRoute = require("./routes/register");
const itemsRoute = require("./routes/items");
const borrowRoute = require("./routes/borrow");
const session = require("express-session");
const MemoryStore = require("memorystore")(session);

// database connection
const con = require("./config/db");
const app = express();
//set "public" folder to be static folder, user can access it directly
app.use("/public", express.static(path.join(__dirname, "public")));

// for json exchange
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// ============= Pages ==============
// ============= Create hashed password ==============
app.get("/password/:pass", function (req, res) {
  const password = req.params.pass;
  const saltRounds = 10; //the cost of encrypting see https://github.com/kelektiv/node.bcrypt.js#a-note-on-rounds

  bcrypt.hash(password, saltRounds, function (err, hash) {
    if (err) {
      return res.status(500).send("Hashing error");
    }
    //return hashed password, 60 characters
    // console.log(hash.length);
    res.send(hash);
  });
});

// ============= Pages ==============
app.use(
  session({
    cookie: { maxAge: 24 * 60 * 60 * 1000 }, //1 day in millisec
    secret: "keyboardcat",
    resave: false,
    saveUninitialized: true,
    store: new MemoryStore({
      checkPeriod: 24 * 60 * 60 * 1000, // prune expired entries every 24h
    }),
  })
);

app.use(userRoute);
app.use(staffRoute);
app.use(lenderRoute);
app.use(registerRoute);
app.use(loginRoute);
app.use(itemsRoute);
app.use(borrowRoute);

// ============= Root ==============
app.get("/", function (req, res) {
    // console.error(err.message);
    // res.status(500).send("Cannot clear session");
  if (req.session.Role == "user") {
    return res.redirect('/user/home');
  }
  if (req.session.Role == "staff") {
    return res.redirect('/staff/home');
  }
  if (req.session.Role == "lender") {
    return res.redirect('/lender/home');
  }
  res.sendFile(path.join(__dirname, "/views/Main/welcome.html"));
});

const port = 3000;
app.listen(port, function () {
  console.log("Server is ready at " + port);
});
